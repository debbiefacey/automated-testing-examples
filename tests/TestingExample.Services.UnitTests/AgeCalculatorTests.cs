﻿using FakeItEasy;
using Microsoft.Extensions.Internal;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TestingExample.Services.UnitTests
{
    public class AgeCalculatorTests
    {
        [Fact]
        public void CalculatesAge()
        {
            // arrange
            var birthDate = new DateTime(2000, 6, 1);
            var clock = A.Fake<ISystemClock>();
            A.CallTo(() => clock.UtcNow).Returns(new DateTime(2018, 1, 1));
            var expectedAge = 17;

            var sut = new AgeCalculator(clock);

            // act
            var actualAge = sut.GetAge(birthDate);

            // assert
            Assert.Equal(expectedAge, actualAge);
        }
    }
}
