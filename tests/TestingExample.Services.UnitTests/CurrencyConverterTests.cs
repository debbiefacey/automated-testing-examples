﻿using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Text;
using TestingExample.Data;
using Xunit;

namespace TestingExample.Services.UnitTests
{
    public class CurrencyConverterTests
    {
        [Fact]
        public void CalculatesJMDAmountGivenSourceCurrencyAndAmount()
        {
            // arrange
            var sourceCurrency = "USD";
            var amount = 5.00m;
            var expectedResult = 635.15m;

            var exchangeRateRepository = A.Fake<IExchangeRateRepository>();
            A.CallTo(() => exchangeRateRepository.GetExchangeRate(sourceCurrency))
                .Returns(new ExchangeRate { JmdRate = 127.03m });
            A.CallTo(() => exchangeRateRepository.GetExchangeRate("JMD"))
                .Returns(new ExchangeRate { JmdRate = 1.00m });

            var sut = new CurrencyConverter(exchangeRateRepository);

            // act
            var result = sut.Convert(amount, sourceCurrency);

            // assert
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void CalculatesDestinationAmountGivenSourceCurrencyAndAmount()
        {
            // arrange
            var sourceCurrency = "USD";
            var destinationCurrency = "CAD";
            var amount = 500m;
            var expectedResult = 1000m;

            var exchangeRateRepository = A.Fake<IExchangeRateRepository>();

            A.CallTo(() => exchangeRateRepository.GetExchangeRate(sourceCurrency))
                .Returns(new ExchangeRate { JmdRate = 100m });

            A.CallTo(() => exchangeRateRepository.GetExchangeRate(destinationCurrency))
                .Returns(new ExchangeRate { JmdRate = 50m });

            var sut = new CurrencyConverter(exchangeRateRepository);

            // act
            var result = sut.Convert(amount, sourceCurrency, destinationCurrency);

            // assert
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [ClassData(typeof(RespositoryData))]
        public void ThrowsNotsupportedExceptionWhenCurrencyNotSupport(
            IExchangeRateRepository exchangeRateRepository)
        {
            // arrange
            var sourceCurrency = "USD";
            var destinationCurrency = "CAD";
            var amount = 500m;
                        
            var sut = new CurrencyConverter(exchangeRateRepository);

            // act
            Action callConvert = () => sut.Convert(amount, sourceCurrency, destinationCurrency);

            // assert
            Assert.Throws<NotSupportedException>(callConvert);
        }

        [Fact]
        public void AddsCurrencyToRespository()
        {
            // arrange
            var currency = "GBP";
            var rate = 200m;

            var exchangeRateRepository = A.Fake<IExchangeRateRepository>();

            var sut = new CurrencyConverter(exchangeRateRepository);

            // act
            sut.AddCurrency(currency, rate);

            // assert
            A.CallTo(
                () => exchangeRateRepository.AddExchangeRate(
                    A<ExchangeRate>.That.Matches(
                        x => x.CurrencyCode == currency && x.JmdRate == rate,
                        $"Currency: {currency}, Rate: {rate}")))
             .MustHaveHappened(Repeated.Exactly.Once);
        }

        public class RespositoryData: TheoryData<IExchangeRateRepository>
        {
            public RespositoryData()
            {
                AddRepositoryWithNoCurrencies();

                AddRepositoryWithUSDCurrencyOnly();

                AddRepositoryWithCADCurrencyOnly();
            }

            private void AddRepositoryWithNoCurrencies()
            {
                var exchangeRateRepository = A.Fake<IExchangeRateRepository>();

                A.CallTo(() => exchangeRateRepository.GetExchangeRate(A<string>.Ignored))
                   .Returns(null);

                Add(exchangeRateRepository);
            }

            private void AddRepositoryWithUSDCurrencyOnly()
            {
                var exchangeRateRepository = A.Fake<IExchangeRateRepository>();

                A.CallTo(() => exchangeRateRepository.GetExchangeRate("USD"))
                   .Returns(new ExchangeRate());

                A.CallTo(() => exchangeRateRepository.GetExchangeRate("CAD"))
                  .Returns(null);

                Add(exchangeRateRepository);
            }

            private void AddRepositoryWithCADCurrencyOnly()
            { 
                var exchangeRateRepository = A.Fake<IExchangeRateRepository>();

                A.CallTo(() => exchangeRateRepository.GetExchangeRate("CAD"))
                   .Returns(new ExchangeRate());

                A.CallTo(() => exchangeRateRepository.GetExchangeRate("USD"))
                 .Returns(null);

                Add(exchangeRateRepository);
            }
        }            
    }
}
