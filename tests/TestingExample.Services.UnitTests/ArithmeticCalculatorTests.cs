﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TestingExample.Services.UnitTests
{
    public class ArithmeticCalculatorTests
    {
        ArithmeticCalculator sut;

        public ArithmeticCalculatorTests()
        {
            sut = new ArithmeticCalculator();
        }

        [Theory]     
        [MemberData(nameof(AddData))]
        public void AddsOperands(double operand1, double operand2, double expected)
        {            
            // act
            var result = sut.Add(operand1, operand2);

            // assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void ThrowsExceptionWhenAddResultOutOfRange()
        {
            Assert.Throws<ArithmeticException>(() => sut.Add(double.MaxValue, double.MaxValue));
        }

        public static TheoryData<double, double, double> AddData =>
            new TheoryData<double, double, double>
            {
                { 2, 3, 5 },
                { -2, 3, 1 },
                { -2, -3, -5 }
            };
    }
}
