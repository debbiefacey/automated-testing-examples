using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Xunit;

namespace TestingExample.Data.UnitTests
{
    public class ExchangeRateRepositoryTests
    {
        [Fact]
        public void AddsExchangeRateToDatabase()
        {
            // arrange and act

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AddsExchangeRateToDatabase")
                .Options;

            // Run the test against one instance of the context
            using (var context = new ApplicationDbContext(options))
            {
                var sut = new ExchangeRateRepository(context);
                sut.AddExchangeRate(new ExchangeRate { CurrencyCode = "JMD", JmdRate = 1 });
            }

            // assert

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = new ApplicationDbContext(options))
            {
                Assert.Equal(1, context.ExchangeRates.Count());
                Assert.Equal("JMD", context.ExchangeRates.Single().CurrencyCode);
            }
        }
    }
}
