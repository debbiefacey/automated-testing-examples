﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestingExample.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExchangeRates",
                columns: table => new
                {
                    CurrencyCode = table.Column<string>(maxLength: 3, nullable: false),
                    JmdRate = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExchangeRates", x => x.CurrencyCode);
                });

            migrationBuilder.InsertData(
                table: "ExchangeRates",
                columns: new[] { "CurrencyCode", "JmdRate" },
                values: new object[] { "JMD", 1.00m });

            migrationBuilder.InsertData(
                table: "ExchangeRates",
                columns: new[] { "CurrencyCode", "JmdRate" },
                values: new object[] { "USD", 127.03m });

            migrationBuilder.InsertData(
                table: "ExchangeRates",
                columns: new[] { "CurrencyCode", "JmdRate" },
                values: new object[] { "CAD", 98.97m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExchangeRates");
        }
    }
}
