﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestingExample.Data
{
    public class ExchangeRateRepository : IExchangeRateRepository
    {
        private readonly ApplicationDbContext db;

        public ExchangeRateRepository(ApplicationDbContext db)
        {
            this.db = db;
        }

        public void AddExchangeRate(ExchangeRate exchangeRate)
        {
            db.ExchangeRates.Add(exchangeRate);

            db.SaveChanges();
        }

        public ExchangeRate GetExchangeRate(string currencyCode)
        {
            return db.ExchangeRates.Find(currencyCode);
        }
    }
}
