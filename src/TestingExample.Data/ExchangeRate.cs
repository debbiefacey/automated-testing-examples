﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestingExample.Data
{
    public class ExchangeRate
    {
        [Key]
        [Required]
        [StringLength(3, MinimumLength = 3)]
        public string CurrencyCode { get; set; }
                     
        public decimal JmdRate { get; set; }
    }
}
