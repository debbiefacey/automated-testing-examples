﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestingExample.Data
{
    public interface IExchangeRateRepository
    {
        ExchangeRate GetExchangeRate(string currencyCode);

        void AddExchangeRate(ExchangeRate exchangeRate);
    }
}
