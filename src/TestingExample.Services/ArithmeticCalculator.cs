﻿using System;

namespace TestingExample.Services
{
    public class ArithmeticCalculator
    {
        public double Add(double operand1, double operand2)
        {
            var result = operand1 + operand2;

            if (double.IsInfinity(result))
            {
                throw new ArithmeticException("Infinity was returned");
            }

            return result;
        }
    }
}
