﻿using Microsoft.Extensions.Internal;
using System;

namespace TestingExample.Services
{
    public class AgeCalculator
    {
        private readonly ISystemClock clock;

        public AgeCalculator(ISystemClock clock)
        {
            this.clock = clock;
        }

        public int GetAge(DateTime birthdate)
        {
            // Save today's date.
            var today = clock.UtcNow.ToLocalTime().Date;
            // Calculate the age.
            var age = today.Year - birthdate.Year;
            // Go back to the year the person was born in case of a leap year
            if (birthdate > today.AddYears(-age)) age--;

            return age;
        }
    }
}
