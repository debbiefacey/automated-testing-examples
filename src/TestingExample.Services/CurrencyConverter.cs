﻿using System;
using System.Collections.Generic;
using System.Text;
using TestingExample.Data;

namespace TestingExample.Services
{
    public class CurrencyConverter
    {
        private readonly IExchangeRateRepository exchangeRateRepository;

        public CurrencyConverter(IExchangeRateRepository exchangeRateRepository)
        {
            this.exchangeRateRepository = exchangeRateRepository;
        }

        public decimal Convert(decimal amount, string from, string to = "JMD")
        {
            var fromRate = 
                exchangeRateRepository.GetExchangeRate(from) ?? throw new NotSupportedException();
            var toRate = 
                exchangeRateRepository.GetExchangeRate(to) ?? throw new NotSupportedException();

            return amount * fromRate.JmdRate / toRate.JmdRate;
        }

        public void AddCurrency(string currencyCode, decimal rate)
        {
            var exchangeRate = new ExchangeRate
            {
                CurrencyCode = currencyCode,
                JmdRate = rate
            };

            exchangeRateRepository.AddExchangeRate(exchangeRate);
        }
    }
}
