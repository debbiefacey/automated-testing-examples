﻿using Microsoft.AspNetCore.Mvc;
using TestingExample.Data;

namespace TestingExample.Api.Controllers
{
    [Route("api/exchange-rates")]
    [ApiController]
    public class ExchangeRatesController : ControllerBase
    {
        private readonly IExchangeRateRepository exchangeRateRepository;

        public ExchangeRatesController(IExchangeRateRepository exchangeRateRepository)
        {
            this.exchangeRateRepository = exchangeRateRepository;
        }

        [HttpGet("{currencyCode}")]
        [ProducesResponseType(200, Type = typeof(ExchangeRate))]
        [ProducesResponseType(404)]
        public IActionResult GetExchangeRate([FromRoute]string currencyCode)
        {
            var rate = exchangeRateRepository.GetExchangeRate(currencyCode);

            if (rate == null) return NotFound(currencyCode);

            return Ok(rate);
        }
    }
}